package daoTest;

import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;

public class TagDaoTest extends DatabaseTestCase {
	
	private TagDao tagDao;
	private Connection jdbcConnection;
	private IDataSet loadedDataSet;

	public TagDao getTagDao() {
		return this.tagDao;
	}

	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}
	
	@Before
	protected IDatabaseConnection getConnection() throws Exception {
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("application-context-test.xml");
		setTagDao((TagDao)context.getBean("TagDao"));
		Class.forName("oracle.jdbc.driver.OracleDriver");
		jdbcConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "NEWS_PORTAL",
				"NEWS_PORTAL");
		return new DatabaseConnection(jdbcConnection, "NEWS_PORTAL");
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@SuppressWarnings("deprecation")
	@Before
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("dataset/tagDataset.xml"));
		return loadedDataSet;
	}

	@Test
	public void testCreate() throws DaoException {
		int size = tagDao.getAll().size();
		tagDao.create(new Tag(0, "tag"));
		assertTrue(size < tagDao.getAll().size());
	}
	
	@Test
	public void testGetById() throws DaoException {
		Tag tag = new Tag(0, "tag");
		tagDao.create(tag);
		tag.setTagId(tagDao.getLastInsertedId());
		Tag found = tagDao.getById(tag.getTagId());
		assertEquals(found, tag);
	}
	
	@Test
	public void testUpdate() throws DaoException {
		Tag tag = new Tag(0, "tag");
		tagDao.create(tag);
		int lastId = tagDao.getLastInsertedId();
		tag.setTagId(lastId);
		tag.setTagName("updated");
		tagDao.update(tag);
		Tag found = tagDao.getById(tag.getTagId());
		assertEquals("updated", found.getTagName());
	}
	
	@Test
	public void testDeleteById() throws DaoException {
		Tag tag = new Tag(0, "tag");
		tagDao.create(tag);
		tag.setTagId(tagDao.getLastInsertedId());
		assertEquals(tag, tagDao.getById(tag.getTagId()));
		tagDao.deleteById(tag.getTagId());
		assertNull(tagDao.getById(tag.getTagId()));
	}
	
}