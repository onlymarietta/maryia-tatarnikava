package daoTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Timestamp;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.DaoException;

public class CommentDaoTest extends DatabaseTestCase {
	
	private CommentDao commentDao;
	private Connection jdbcConnection;
	private IDataSet loadedDataSet;

	public CommentDao getCommentDao() {
		return this.commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	@Before
	protected IDatabaseConnection getConnection() throws Exception {
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("application-context-test.xml");
		setCommentDao((CommentDao)context.getBean("CommentDao"));
		Class.forName("oracle.jdbc.driver.OracleDriver");
		jdbcConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "NEWS_PORTAL",
				"NEWS_PORTAL");
		return new DatabaseConnection(jdbcConnection, "NEWS_PORTAL");
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@SuppressWarnings("deprecation")
	@Before
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("dataset/commentDataset.xml"));
		return loadedDataSet;
	}

	@Test
	public void testCreate() throws DaoException {
		Comment comment = commentDao.getById(commentDao.getLastInsertedId());
		int size = commentDao.getAll(comment.getNewsId()).size();
		Comment added = new Comment(0, comment.getNewsId(), "comment", new Timestamp(200));
		commentDao.create(added);
		assertTrue(size < commentDao.getAll(comment.getNewsId()).size());
	}
	
	@Test
	public void testGetById() throws DaoException {
		Comment comment = commentDao.getById(commentDao.getLastInsertedId());
		Comment added = new Comment(0, comment.getNewsId(), "comment", new Timestamp(200));
		commentDao.create(added);
		added.setCommentId(commentDao.getLastInsertedId());
		Comment found = commentDao.getById(added.getCommentId());
		assertEquals(found, added);
	}
	
	@Test
	public void testUpdate() throws DaoException {
		Comment comment = commentDao.getById(commentDao.getLastInsertedId());
		Comment added = new Comment(0, comment.getNewsId(), "comment", new Timestamp(200));
		commentDao.create(added);
		added.setCommentId(commentDao.getLastInsertedId());
		added.setCommentText("updated");
		commentDao.update(added);
		Comment found = commentDao.getById(added.getCommentId());
		assertEquals("updated", found.getCommentText());
	}
	
	@Test
	public void testDeleteById() throws DaoException {
		Comment comment = commentDao.getById(commentDao.getLastInsertedId());
		Comment added = new Comment(0, comment.getNewsId(), "comment", new Timestamp(200));
		commentDao.create(added);
		added.setCommentId(commentDao.getLastInsertedId());
		assertEquals(added, commentDao.getById(commentDao.getLastInsertedId()));
		commentDao.deleteById(added.getCommentId());
		assertNull(commentDao.getById(added.getCommentId()));
	}
	
}