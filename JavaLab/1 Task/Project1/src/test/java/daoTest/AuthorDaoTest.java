package daoTest;

import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.DaoException;

public class AuthorDaoTest extends DatabaseTestCase {
	
	private AuthorDao authorDao;
	private Connection jdbcConnection;
	private IDataSet loadedDataSet;

	public AuthorDao getAuthorDao() {
		return this.authorDao;
	}

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}
	
	@Before
	protected IDatabaseConnection getConnection() throws Exception {
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("application-context-test.xml");
		setAuthorDao((AuthorDao)context.getBean("AuthorDao"));
		Class.forName("oracle.jdbc.driver.OracleDriver");
		jdbcConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "NEWS_PORTAL",
				"NEWS_PORTAL");
		return new DatabaseConnection(jdbcConnection, "NEWS_PORTAL");
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@SuppressWarnings("deprecation")
	@Before
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("dataset/authorDataset.xml"));
		return loadedDataSet;
	}

	@Test
	public void testCreate() throws DaoException {
		int size = authorDao.getAll(true).size();
		authorDao.create(new Author(0, "author", null));
		assertTrue(size < authorDao.getAll(true).size());
	}
	
	@Test
	public void testGetById() throws DaoException {
		Author author = new Author(0, "author", null);
		authorDao.create(author);
		author.setAuthorId(authorDao.getLastInsertedId());
		Author found = authorDao.getById(author.getAuthorId());
		assertEquals(found, author);
	}
	
	@Test
	public void testUpdate() throws DaoException {
		Author author = new Author(0, "author", null);
		authorDao.create(author);
		int lastId = authorDao.getLastInsertedId();
		author.setAuthorId(lastId);
		author.setAuthorName("updated");
		authorDao.update(author);
		Author found = authorDao.getById(author.getAuthorId());
		assertEquals("updated", found.getAuthorName());
	}
	
	@Test
	public void testDeleteById() throws DaoException {
		Author author = new Author(0, "author", null);
		authorDao.create(author);
		author.setAuthorId(authorDao.getLastInsertedId());
		assertEquals(author, authorDao.getById(author.getAuthorId()));
		authorDao.deleteById(author.getAuthorId());
		assertNull(authorDao.getById(author.getAuthorId()));
	}
	
}