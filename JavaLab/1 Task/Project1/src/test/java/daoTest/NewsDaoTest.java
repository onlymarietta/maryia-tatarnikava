package daoTest;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.Timestamp;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.exception.DaoException;

public class NewsDaoTest extends DatabaseTestCase {
	
	private NewsDao newsDao;
	private Connection jdbcConnection;
	private IDataSet loadedDataSet;

	public NewsDao getNewsDao() {
		return this.newsDao;
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}
	
	@Before
	protected IDatabaseConnection getConnection() throws Exception {
		ApplicationContext context = 
	             new ClassPathXmlApplicationContext("application-context-test.xml");
		setNewsDao((NewsDao)context.getBean("NewsDao"));
		Class.forName("oracle.jdbc.driver.OracleDriver");
		jdbcConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "NEWS_PORTAL",
				"NEWS_PORTAL");
		return new DatabaseConnection(jdbcConnection, "NEWS_PORTAL");
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.REFRESH;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	@SuppressWarnings("deprecation")
	@Before
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("dataset/newsDataset.xml"));
		return loadedDataSet;
	}

	@Test
	public void testCreate() throws DaoException {
		int size = newsDao.getAll().size();
		newsDao.create(new News(0, "title", "short", "full", new Timestamp(100), new Date(100)));
		assertTrue(size < newsDao.getAll().size());
	}
	
	@Test
	public void testGetById() throws DaoException {
		News news = new News(0, "title", "short", "full", new Timestamp(100), new Date(100));
		newsDao.create(news);
		news.setNewsId(newsDao.getLastInsertedId());
		News found = newsDao.getById(news.getNewsId());
		assertEquals(found, news);
	}
	
	@Test
	public void testUpdate() throws DaoException {
		News news = new News(0, "title", "short", "full", new Timestamp(100), new Date(100));
		newsDao.create(news);
		int lastId = newsDao.getLastInsertedId();
		news.setNewsId(lastId);
		news.setTitle("updated");
		newsDao.update(news);
		News found = newsDao.getById(news.getNewsId());
		assertEquals("updated", found.getTitle());
	}
	
	@Test
	public void testDeleteById() throws DaoException {
		News news = new News(0, "title", "short", "full", new Timestamp(100), new Date(100));
		newsDao.create(news);
		news.setNewsId(newsDao.getLastInsertedId());
		assertEquals(news, newsDao.getById(newsDao.getLastInsertedId()));
		newsDao.deleteById(news.getNewsId());
		assertNull(newsDao.getById(news.getNewsId()));
	}
	
}