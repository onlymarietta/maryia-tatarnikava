package serviceTest;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDao authorDao;
	
	private AuthorService authorService;
	
	@Before
	public void setUp() {
		authorDao = mock(AuthorDao.class);
		authorService = new AuthorServiceImpl();
		authorService.setAuthorDao(authorDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		Author author = new Author(0, "author", null);
		authorService.create(author);
		verify(authorDao).create(author);
	}
	
	@Test
	public void testUpdate() throws DaoException, ServiceException {
		Author author = new Author(0, "author", null);
		authorService.update(author);
		verify(authorDao).update(author);
	}
	
	@Test 
	public void testExpire() throws DaoException, ServiceException {
		Author author = new Author(0, "author", null);
		authorService.expire(author, new Timestamp(200));
		verify(authorDao).update(author);;
	}
	
	@Test
	public void testGetAll() throws DaoException, ServiceException {
		List<Author> authorList = new ArrayList<Author>();
		authorList.add(new Author(0, "author21", null));
		authorList.add(new Author(0, "author22", null));
		when(authorDao.getAll(true)).thenReturn(authorList);
		authorService.getAll(true);
		verify(authorDao).getAll(true);
	}
	
}