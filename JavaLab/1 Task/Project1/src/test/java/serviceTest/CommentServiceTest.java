package serviceTest;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private CommentDao commentDao;
	
	private CommentService commentService;
	
	@Before
	public void setUp() {
		commentDao = mock(CommentDao.class);
		commentService = new CommentServiceImpl();
		commentService.setCommentDao(commentDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		Comment comment = new Comment(0, 1, "comment", new Timestamp(200));
		commentService.create(comment);
		verify(commentDao).create(comment);
	}
	
	@Test 
	public void testDelete() throws DaoException, ServiceException {
		int commentId = 0;
		commentService.delete(commentId);
		verify(commentDao).deleteById(commentId);
	}
	
	@Test
	public void testGetAll() throws DaoException, ServiceException {
		int newsId = 1;
		List<Comment> commentList = new ArrayList<Comment>();
		commentList.add(new Comment(0, newsId, "comment20", new Timestamp(200)));
		commentList.add(new Comment(0, newsId, "comment21", new Timestamp(200)));
		when(commentDao.getAll(newsId)).thenReturn(commentList);
		commentService.getAll(newsId);
		verify(commentDao).getAll(newsId);
	}
	
}