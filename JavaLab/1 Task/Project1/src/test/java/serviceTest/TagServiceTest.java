package serviceTest;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private TagDao tagDao;
	
	private TagService tagService;
	
	@Before
	public void setUp() {
		tagDao = mock(TagDao.class);
		tagService = new TagServiceImpl();
		tagService.setTagDao(tagDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		Tag tag = new Tag(0, "abc");
		tagService.create(tag);
		verify(tagDao).create(tag);
	}
	
	@Test
	public void testUpdate() throws DaoException, ServiceException {
		Tag tag = new Tag(0, "abc");
		tagService.update(tag);
		verify(tagDao).update(tag);
	}
	
	@Test 
	public void testDelete() throws DaoException, ServiceException {
		int tagId = 0;
		tagService.delete(tagId);
		verify(tagDao).deleteById(tagId);
	}
	
	@Test
	public void testGetAll() throws DaoException, ServiceException {
		List<Tag> tagList = new ArrayList<Tag>();
		tagList.add(new Tag(0, "abc"));
		tagList.add(new Tag(0, "def"));
		when(tagDao.getAll()).thenReturn(tagList);
		tagService.getAll();
		verify(tagDao).getAll();
	}
	
}