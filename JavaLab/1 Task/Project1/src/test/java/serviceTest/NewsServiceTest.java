package serviceTest;

import static org.mockito.Mockito.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.SearchCriteriaObj;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDao newsDao;
	@Mock
	private AuthorDao authorDao;
	@Mock
	private CommentDao commentDao;
	@Mock
	private TagDao tagDao;
	
	private NewsService newsService;
	
	@Before
	public void setUp() {
		newsDao = mock(NewsDao.class);
		authorDao = mock(AuthorDao.class);
		commentDao = mock(CommentDao.class);
		tagDao = mock(TagDao.class);
		
		newsService = new NewsServiceImpl();
		newsService.setNewsDao(newsDao);
		newsService.setAuthorDao(authorDao);
		newsService.setCommentDao(commentDao);
		newsService.setTagDao(tagDao);
	}

	@Test
	public void testCreate() throws DaoException, ServiceException {
		News news = new News(0, "title", "short", "full", new Timestamp(100), new Date(100));
		Author author = new Author(0, "author", null);
		List<Tag> listTag = new ArrayList<Tag>();
		listTag.add(new Tag(0, "abc"));
		listTag.add(new Tag(0, "def"));
		newsService.create(news, author, listTag);
		verify(newsDao).create(news);
		verify(newsDao).addAuthor(news.getNewsId(), author.getAuthorId());
		verify(newsDao).addTags(news.getNewsId(), listTag);
	}
	
	@Test
	public void testRead() throws DaoException, ServiceException {
		int newsId = 1;
		newsService.read(newsId);
		verify(newsDao).getById(newsId);
		verify(newsDao).getAuthor(newsId);
		verify(commentDao).getAll(newsId);
		verify(tagDao).getAll(newsId);
	}
	
	@Test
	public void testUpdate() throws DaoException, ServiceException {
		News news = new News(0, "title", "short", "full", new Timestamp(100), new Date(100));
		List<Tag> listTag = new ArrayList<Tag>();
		listTag.add(new Tag(0, "abc"));
		listTag.add(new Tag(0, "def"));
		newsService.update(news, listTag);
		verify(newsDao).update(news);
		verify(newsDao).deleteAllTags(news.getNewsId());
		verify(newsDao).addTags(news.getNewsId(), listTag);
	}
	
	@Test 
	public void testDelete() throws DaoException, ServiceException {
		int newsId = 1;
		newsService.delete(newsId);
		verify(newsDao).deleteById(newsId);
	}
	
	@Test
	public void testGetAll() throws DaoException, ServiceException {
		newsService.getAll();
		verify(newsDao).getAll();
	}
	
	@Test 
	public void testListMostCommentedNews() throws DaoException, ServiceException {
		newsService.listMostCommentedNews();
		verify(newsDao).getAll();
	}
	
	@Test
	public void testCountAll() throws DaoException, ServiceException {
		newsService.countAll();
		verify(newsDao).countAll();
	}
	
	@Test
	public void testFindByCriteria() throws DaoException, ServiceException {
		SearchCriteriaObj criteria = new SearchCriteriaObj();
		Author author = new Author(0,  "author", null);
		List<Tag> listTag = new ArrayList<Tag>();
		listTag.add(new Tag(0, "abc"));
		listTag.add(new Tag(0, "def"));
		criteria.setAuthor(author);
		criteria.setListTag(listTag);
		newsService.findByCriteria(criteria);
		verify(newsDao).findByCriteria(criteria);
	}
	
}