package com.epam.newsmanagement.entity;

import java.util.List;

import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.Tag;

/**
 * Class contains properties of search criteria
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class SearchCriteriaObj {
	private Author author;
	private List<Tag> listTag;

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getListTag() {
		return listTag;
	}

	public void setListTag(List<Tag> listTag) {
		this.listTag = listTag;
	}

}
