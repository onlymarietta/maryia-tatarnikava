package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface allows to perform operations with tags in database
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface TagDao extends BaseDao<Tag> {
	
	/**
	 * Returns all tags
	 * @return list of all tags
	 * @throws DaoException
	 */
	List<Tag> getAll() throws DaoException;

	/**
	 * Returns all tags of one news
	 * @param newsId is id of the news
	 * @return list of tags
	 * @throws DaoException
	 */
	List<Tag> getAll(int newsId) throws DaoException;

}
