package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.DaoException;

public class CommentDaoImpl implements CommentDao {

	private static final String SQL_CREATE = "insert into COMMENTS (NEWS_ID, COMMENT_TEXT, CREATION_DATE) "
			+ "values (?, ?, ?)";
	private static final String SQL_GET_BY_ID = "select COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE"
			+ " from COMMENTS where COMMENT_ID = ?";
	private static final String SQL_UPDATE = "update COMMENTS set NEWS_ID=?, COMMENT_TEXT=?, CREATION_DATE=? "
			+ "where COMMENT_ID=?";
	private static final String SQL_DELETE = "delete from COMMENTS where COMMENT_ID = ?";
	private static final String SQL_GET_LAST_INSERTED_ID = "select max(COMMENT_ID) from COMMENTS";
	private static final String SQL_GET_ALL = "select COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE "
			+ "from COMMENTS where NEWS_ID = ?";
	private static final String SQL_COUNT_ALL = "SELECT COUNT (COMMENT_ID) FROM COMMENTS where NEWS_ID = ?";

	private static final String COLUMN_COMMENT_ID = "COMMENT_ID";
	private static final String COLUMN_NEWS_ID = "NEWS_ID";
	private static final String COLUMN_COMMENT_TEXT = "COMMENT_TEXT";
	private static final String COLUMN_CREATION_DATE = "CREATION_DATE";

	private DataSource dataSource;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void create(Comment comment) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_CREATE)) {
			ps.setInt(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, comment.getCreationDate());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't create comment: " + ex.getMessage(), ex);
		}
	}

	@Override
	public Comment getById(Integer id) throws DaoException {
		Comment comment = new Comment();
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_BY_ID)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			comment = mapper(rs);
			rs.close();
			return comment;
		} catch (SQLException ex) {
			throw new DaoException("Can't get comment by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void update(Comment comment) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_UPDATE)) {
			ps.setInt(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, comment.getCreationDate());
			ps.setInt(4, comment.getCommentId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't update comment: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void deleteById(Integer id) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_DELETE)) {
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't delete comment by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public int getLastInsertedId() throws DaoException {
		int insertedId = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_LAST_INSERTED_ID)) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			insertedId = rs.getInt(1);
			return insertedId;

		} catch (SQLException ex) {
			throw new DaoException("Can't get last inserted comment id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public List<Comment> getAll(int newsId) throws DaoException {
		List<Comment> listComment = new ArrayList<Comment>();
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_GET_ALL)) {
			ps.setInt(1, newsId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listComment.add(mapper(rs));
			}
			rs.close();
			return listComment;
		} catch (SQLException ex) {
			throw new DaoException("Can't get all comments: " + ex.getMessage(), ex);
		}
	}

	@Override
	public int countAll(int newsId) throws DaoException {
		int count = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_COUNT_ALL)) {
			ps.setInt(1, newsId);
			ResultSet rs = ps.executeQuery();
			rs.next();
			count = rs.getInt(1);
			rs.close();
			return count;

		} catch (SQLException ex) {
			throw new DaoException("Can't count all comments for ine news: " + ex.getMessage(), ex);
		}
	}

	private Comment mapper(ResultSet rs) throws DaoException {
		try {
			Comment comment = new Comment();
			comment.setCommentId(rs.getInt(COLUMN_COMMENT_ID));
			comment.setNewsId(rs.getInt(COLUMN_NEWS_ID));
			comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
			comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
			return comment;
		} catch (SQLException ex) {
			throw new DaoException("Can't map into comment: " + ex.getMessage(), ex);
		}
	}

}
