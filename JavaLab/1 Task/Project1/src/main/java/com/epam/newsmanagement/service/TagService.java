package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface allows to perform operations with tags
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface TagService {
	
	/**
	 * Sets object of tag dao
	 * @param tagDao is object to set
	 */
	void setTagDao(TagDao tagDao);
	
	/**
	 * Creates new tag
	 * @param tag to create
	 * @throws ServiceException
	 */
	void create(Tag tag) throws ServiceException;

	/**
	 * Updates tag
	 * @param tag to update
	 * @throws ServiceException
	 */
	void update(Tag tag) throws ServiceException;
	
	/**
	 * Deletes tag
	 * @param tagId is id of tag to delete
	 * @throws ServiceException
	 */
	void delete(int tagId) throws ServiceException;
	
	/**
	 * Returnes all tags
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> getAll() throws ServiceException;

}
