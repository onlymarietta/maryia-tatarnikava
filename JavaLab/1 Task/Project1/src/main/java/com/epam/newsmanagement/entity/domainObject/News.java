package com.epam.newsmanagement.entity.domainObject;

import java.sql.Timestamp;
import java.sql.Date;

/**
 * Class contains properties of news
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class News extends BaseEntity{
	private int newsId;
	private String title;
	private String shortText;
	private String fullText;
	private Timestamp creationDate;
	private Date modificationDate;
	
	public News() {}
	
	public News (int newsId, String title, String shortText, String fullText, 
			Timestamp creationDate, Date modificationDate) {
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	public boolean equals(Object obj) {
		News news = (News)obj;
		return this.newsId == news.getNewsId() && this.title.equals(news.getTitle()) 
				&& this.shortText.equals(news.getShortText()) && this.fullText.equals(news.getFullText())
				&& this.creationDate.toString().equals(news.getCreationDate().toString()) 
				&& this.modificationDate.toString().equals(news.getModificationDate().toString());
	}
	
	public String toString() {
		return this.newsId + " " + this.title + " " + this.shortText + " " + this.fullText + " " + this.creationDate.toString()
				+ " " + this.modificationDate.toString();
	}
	
}
