package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.DaoException;

public class AuthorDaoImpl implements AuthorDao {

	private static final String SQL_CREATE = "insert into AUTHOR (AUTHOR_NAME, EXPIRED) values (?, ?)";
	private static final String SQL_GET_BY_ID = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_ID = ?";
	private static final String SQL_UPDATE = "update AUTHOR set AUTHOR_NAME=?, EXPIRED=? where AUTHOR_ID=?";
	private static final String SQL_DELETE_AUTHOR = "delete from AUTHOR where AUTHOR_ID = ?";
	private static final String SQL_DELETE_NEWS_AUTHOR = "delete from NEWS_AUTHOR where AUTHOR_ID = ?";
	private static final String SQL_GET_LAST_INSERTED_ID = "select max(AUTHOR_ID) from AUTHOR";
	private static final String SQL_GET_ALL = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR";
	private static final String SQL_GET_ALL_EXCEPT_EXPIRED = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR "
			+ "where EXPIRED = NULL";

	private static final String COLUMN_AUTHOR_ID = "AUTHOR_ID";
	private static final String COLUMN_AUTHOR_NAME = "AUTHOR_NAME";
	private static final String COLUMN_EXPIRED = "EXPIRED";

	private DataSource dataSource;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void create(Author author) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_CREATE)) {
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't create author: " + ex.getMessage(), ex);
		}

	}

	@Override
	public Author getById(Integer id) throws DaoException {
		Author author = new Author();
		try (Connection conn = DataSourceUtils.getConnection(dataSource);
				PreparedStatement ps = conn.prepareStatement(SQL_GET_BY_ID)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			author = mapper(rs);
			rs.close();
			return author;
		} catch (SQLException ex) {
			throw new DaoException("Can't get author by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void update(Author author) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_UPDATE)) {
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.setInt(3, author.getAuthorId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't update author: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void deleteById(Integer id) throws DaoException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps1 = conn.prepareStatement(SQL_DELETE_AUTHOR);
				PreparedStatement ps2 = conn.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
			ps1.setInt(1, id);
			ps1.executeUpdate();
			ps2.setInt(1, id);
			ps2.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't delete author by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public int getLastInsertedId() throws DaoException {
		int insertedId = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_LAST_INSERTED_ID)) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			insertedId = rs.getInt(1);
			return insertedId;

		} catch (SQLException ex) {
			throw new DaoException("Can't get last inserted author id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public List<Author> getAll(boolean retrieveExpired) throws DaoException {
		String sql;
		if (retrieveExpired)
			sql = SQL_GET_ALL;
		else
			sql = SQL_GET_ALL_EXCEPT_EXPIRED;
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			List<Author> listAuthor = new ArrayList<Author>();
			while (rs.next()) {
				listAuthor.add(mapper(rs));
			}
			rs.close();
			return listAuthor;
		} catch (SQLException ex) {
			throw new DaoException("Can't get all authors:" + ex.getMessage(), ex);
		}
	}

	private Author mapper(ResultSet rs) throws DaoException {
		try {
			Author author = new Author();
			author.setAuthorId(rs.getInt(COLUMN_AUTHOR_ID));
			author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
			author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
			return author;
		} catch (SQLException ex) {
			throw new DaoException("Can't map into author: " + ex.getMessage(), ex);
		}
	}

}
