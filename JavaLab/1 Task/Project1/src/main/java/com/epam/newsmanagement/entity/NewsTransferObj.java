package com.epam.newsmanagement.entity;

import java.util.List;

import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;

/**
 * Class contains news with author, tags and comments
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class NewsTransferObj {
	private News news;
	private Author author;
	private List<Comment> listComment;
	private List<Tag> listTag;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Comment> getListComment() {
		return listComment;
	}

	public void setListComment(List<Comment> listComment) {
		this.listComment = listComment;
	}

	public List<Tag> getListTag() {
		return listTag;
	}

	public void setListTag(List<Tag> listTag) {
		this.listTag = listTag;
	}

}
