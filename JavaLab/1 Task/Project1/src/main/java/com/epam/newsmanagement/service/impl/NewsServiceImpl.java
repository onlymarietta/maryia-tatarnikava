package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.NewsTransferObj;
import com.epam.newsmanagement.entity.SearchCriteriaObj;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utils.NewsComparator;

public class NewsServiceImpl implements NewsService {

	static final Logger logger = LogManager.getLogger(News.class.getName());
	private NewsDao newsDao;
	private AuthorDao authorDao;
	private CommentDao commentDao;
	private TagDao tagDao;

	public NewsDao getNewsDao() {
		return newsDao;
	}

	@Override
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public AuthorDao getAuthorDao() {
		return authorDao;
	}

	@Override
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	public CommentDao getCommentDao() {
		return commentDao;
	}

	@Override
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	public TagDao getTagDao() {
		return tagDao;
	}

	@Override
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	@Override
	public void create(News news, Author author, List<Tag> listTag) throws ServiceException {
		try {
			logger.trace("Creating news");
			newsDao.create(news);
			newsDao.addAuthor(news.getNewsId(), author.getAuthorId());
			newsDao.addTags(news.getNewsId(), listTag);
		} catch (DaoException daoEx) {
			String error = "Can't create news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public NewsTransferObj read(int newsId) throws ServiceException {
		try {
			logger.trace("Reading news with author, tags, comments");
			NewsTransferObj newsTransfer = new NewsTransferObj();
			newsTransfer.setNews(newsDao.getById(newsId));
			newsTransfer.setAuthor(newsDao.getAuthor(newsId));
			newsTransfer.setListComment(commentDao.getAll(newsId));
			newsTransfer.setListTag(tagDao.getAll(newsId));
			return newsTransfer;
		} catch (DaoException daoEx) {
			String error = "Can't read news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void update(News news, List<Tag> listTag) throws ServiceException {
		try {
			logger.trace("Updating news");
			newsDao.update(news);
			newsDao.deleteAllTags(news.getNewsId());
			newsDao.addTags(news.getNewsId(), listTag);
		} catch (DaoException daoEx) {
			String error = "Can't update news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void delete(int newsId) throws ServiceException {
		try {
			logger.trace("Deleting news");
			newsDao.deleteById(newsId);
		} catch (DaoException daoEx) {
			String error = "Can't delete news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<News> getAll() throws ServiceException {
		try {
			logger.trace("Getting all news");
			List<News> newsList = newsDao.getAll();
			return newsList;
		} catch (DaoException daoEx) {
			String error = "Can't get all news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<News> listMostCommentedNews() throws ServiceException {
		try {
			logger.trace("Getting list of the most commented news");
			List<News> listNews = new ArrayList<News>();
			listNews = newsDao.getAll();
			Collections.sort(listNews, new NewsComparator());
			return listNews;
		} catch (DaoException daoEx) {
			String error = "Can't get list of the most commented news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public int countAll() throws ServiceException {
		try {
			logger.trace("Counting all news");
			return newsDao.countAll();
		} catch (DaoException daoEx) {
			String error = "Can't count all news";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<News> findByCriteria(SearchCriteriaObj criteria) throws ServiceException {
		try {
			logger.trace("Searching news by criteria");
			return newsDao.findByCriteria(criteria);
		} catch (DaoException daoEx) {
			String error = "Can't find news by search criteria";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

}
