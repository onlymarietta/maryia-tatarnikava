package com.epam.newsmanagement.entity.domainObject;

import java.sql.Timestamp;

/**
 * Class contains properties of author
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class Author extends BaseEntity {
	private int authorId;
	private String authorName;
	private Timestamp expired;
	
	public Author() {}
	
	public Author(int authorId, String authorName, Timestamp expired) {
		this.authorId = authorId;
		this.authorName = authorName;
		this.expired = expired;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Timestamp getExpired() {
		return expired;
	}

	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	public boolean equals(Object obj) {
		Author author = (Author)obj;
		if(this.expired != null && author.expired != null)
		return (this.authorId == author.getAuthorId()) && (this.authorName.equals(author.getAuthorName())) 
				&& (this.expired.toString().equals(author.expired.toString()) );
		if(this.expired == null && author.expired == null)
			return (this.authorId == author.getAuthorId()) && (this.authorName.equals(author.getAuthorName())) ;
		return false;
	}
	
}
