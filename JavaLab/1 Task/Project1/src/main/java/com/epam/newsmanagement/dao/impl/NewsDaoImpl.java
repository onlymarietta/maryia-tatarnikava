package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.SearchCriteriaObj;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;

public class NewsDaoImpl implements NewsDao {

	private static final String SQL_CREATE = "insert into NEWS (TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE) values (?, ?, ?, ?, ?)";
	private static final String SQL_GET_BY_ID = "select NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE from NEWS where NEWS_ID = ?";
	private static final String SQL_UPDATE = "update NEWS set TITLE=?, SHORT_TEXT=?, FULL_TEXT=?, CREATION_DATE=?, "
			+ "MODIFICATION_DATE=? where NEWS_ID=?";
	private static final String SQL_DELETE_NEWS = "delete from NEWS where NEWS_ID = ?";;
	private static final String SQL_DELETE_NEWS_AUTHOR = "delete from NEWS_AUTHOR where NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS_TAG = "delete from NEWS_TAG where NEWS_ID = ?";;
	private static final String SQL_DELETE_COMMENTS = "delete from COMMENTS where NEWS_ID = ?";;
	private static final String SQL_GET_LAST_INSERTED_ID = "select max(NEWS_ID) from NEWS";
	private static final String SQL_GET_ALL = "select NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, "
			+ "MODIFICATION_DATE from NEWS";
	private static final String SQL_COUNT_ALL = "SELECT COUNT(NEWS_ID) FROM NEWS";
	private static final String SQL_ADD_AUTHOR = "insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (?, ?)";
	private static final String SQL_GET_AUTHOR = "select AUTHOR_ID from NEWS_AUTHOR where NEWS_ID = ?";
	private static final String SQL_ADD_TAG = "into NEWS_TAG (NEWS_ID, TAG_ID) values (?, ?)";
	private static final String SQL_DELETE_ALL_TAGS = "delete from NEWS_TAG where NEWS_ID = ?";
	private static final String SQL_SEARCH_AUTHOR = "select NEWS_ID from NEWS_AUTHOR where AUTHOR_ID = ?";
	private static final String SQL_SEARCH_TAGS = "select NEWS_ID from NEWS_TAG where TAG_ID = ?";
	private static final String SQL_SEARCH_AUTHOR_TAGS = "select NEWS_ID from NEWS_TAG where NEWS_ID in "
			+ "(select NEWS_ID from NEWS_AUTHOR where AUTHOR_ID = ?)";

	private static final String COLUMN_AUTHOR_ID = "AUTHOR_ID";
	private static final String COLUMN_AUTHOR_NAME = "AUTHOR_NAME";
	private static final String COLUMN_AUTHOR_EXPIRED = "EXPIRED";
	private static final String COLUMN_NEWS_ID = "NEWS_ID";
	private static final String COLUMN_TITLE = "TITLE";
	private static final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
	private static final String COLUMN_FULL_TEXT = "FULL_TEXT";
	private static final String COLUMN_CREATION_DATE = "CREATION_DATE";
	private static final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";

	private DataSource dataSource;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void create(News news) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_CREATE)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't create news:" + ex.getMessage(), ex);
		}
	}

	@Override
	public News getById(Integer id) throws DaoException {
		News news = new News();
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_BY_ID)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs == null)
				return null;
			rs.next();
			news = mapper(rs);
			rs.close();
			return news;
		} catch (SQLException ex) {
			throw new DaoException("Can't get news by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void update(News news) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_UPDATE)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.setInt(6, news.getNewsId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't update news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void deleteById(Integer id) throws DaoException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps1 = conn.prepareStatement(SQL_DELETE_NEWS);
				PreparedStatement ps2 = conn.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
				PreparedStatement ps3 = conn.prepareStatement(SQL_DELETE_NEWS_TAG);
				PreparedStatement ps4 = conn.prepareStatement(SQL_DELETE_COMMENTS)) {
			ps1.setInt(1, id);
			ps1.executeUpdate();
			ps2.setInt(1, id);
			ps2.executeUpdate();
			ps3.setInt(1, id);
			ps3.executeUpdate();
			ps4.setInt(1, id);
			ps4.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't delete news by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public int getLastInsertedId() throws DaoException {
		int insertedId = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_LAST_INSERTED_ID)) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			insertedId = rs.getInt(1);
			return insertedId;

		} catch (SQLException ex) {
			throw new DaoException("Can't get last inserted news id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public List<News> getAll() throws DaoException {
		List<News> listNews = new ArrayList<News>();
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_GET_ALL)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listNews.add(mapper(rs));
			}
			rs.close();
			return listNews;
		} catch (SQLException ex) {
			throw new DaoException("Can't get all news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public int countAll() throws DaoException {
		int count = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_COUNT_ALL)) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			count = rs.getInt(1);
			return count;
		} catch (SQLException ex) {
			throw new DaoException("Can't count all news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void addAuthor(int newsId, int authorId) throws DaoException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_ADD_AUTHOR)) {
			ps.setInt(1, newsId);
			ps.setInt(2, authorId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't add author to news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public Author getAuthor(int newsId) throws DaoException {
		Author author = new Author();
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_AUTHOR)) {
			ps.setInt(1, newsId);
			ResultSet rs = ps.executeQuery();
			rs.next();
			author.setAuthorId(rs.getInt(COLUMN_AUTHOR_ID));
			author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
			author.setExpired(rs.getTimestamp(COLUMN_AUTHOR_EXPIRED));
			return author;
		} catch (SQLException ex) {
			throw new DaoException("Can't get author of the news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void addTags(int newsId, List<Tag> listTag) throws DaoException {
		StringBuffer sql = new StringBuffer("insert all ");
		for (int i = 0; i < listTag.size(); i++) {
			sql.append(SQL_ADD_TAG);
		}
		sql.append("select 1 from dual");
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql.toString())) {
			for (int i = 0; i < listTag.size(); i++) {
				ps.setInt(2 * i + 1, newsId);
				ps.setInt((i + 2), listTag.get(i).getTagId());
			}
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't add tags to news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void deleteAllTags(int newsId) throws DaoException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_DELETE_ALL_TAGS)) {
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't delete tags of the news: " + ex.getMessage(), ex);
		}
	}

	@Override
	public List<News> findByCriteria(SearchCriteriaObj criteria) throws DaoException {
		List<News> listNews = new ArrayList<News>();
		String sql = getSearchQuery(criteria);
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
			int beginSetTags = 1;
			if (criteria.getAuthor() != null) {
				ps.setInt(1, criteria.getAuthor().getAuthorId());
				beginSetTags = 2;
			}
			for (int i = 0; i < criteria.getListTag().size(); i++) {
				ps.setInt(beginSetTags + i, criteria.getListTag().get(i).getTagId());
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				News news = this.getById(rs.getInt(COLUMN_NEWS_ID));
				listNews.add(news);
			}
			rs.close();
			return listNews;
		} catch (SQLException ex) {
			throw new DaoException("Can't find news by criteria: " + ex.getMessage(), ex);
		}
	}

	private String getSearchQuery(SearchCriteriaObj criteria) {
		StringBuffer sql = new StringBuffer();
		if (criteria.getAuthor() != null && criteria.getListTag() != null) {
			sql.append(SQL_SEARCH_AUTHOR_TAGS);
			for (int i = 0; i < criteria.getListTag().size(); i++) {
				sql.append(" and  TAG_ID = ?");
			}
		} else {
			if (criteria.getAuthor() != null) {
				sql.append(SQL_SEARCH_AUTHOR);
			}
			if (criteria.getListTag() != null) {
				List<Tag> listTag = criteria.getListTag();
				sql.append(SQL_SEARCH_TAGS);
				for (int i = 1; i < listTag.size(); i++) {
					sql.append(" and TAG_ID = ?");
				}
			}
		}
		return sql.toString();
	}

	private News mapper(ResultSet rs) throws DaoException {
		try {
			News news = new News();
			news.setNewsId(rs.getInt(COLUMN_NEWS_ID));
			news.setTitle(rs.getString(COLUMN_TITLE));
			news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
			news.setFullText(rs.getString(COLUMN_FULL_TEXT));
			news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
			news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
			return news;
		} catch (SQLException ex) {
			throw new DaoException("Can't map news: " + ex.getMessage(), ex);
		}
	}
}
