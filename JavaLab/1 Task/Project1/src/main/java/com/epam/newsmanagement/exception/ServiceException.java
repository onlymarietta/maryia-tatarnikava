package com.epam.newsmanagement.exception;

import java.sql.SQLException;

public class ServiceException extends SQLException {
	
	private static final long serialVersionUID = 8387884195987227513L;
	
	public ServiceException() {
	}

	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(String message, DaoException daoEx) {
		super(message, daoEx);
	}
}
