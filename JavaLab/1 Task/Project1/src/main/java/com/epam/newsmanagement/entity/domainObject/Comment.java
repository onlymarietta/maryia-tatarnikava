package com.epam.newsmanagement.entity.domainObject;

import java.sql.Timestamp;

/**
 * Class contains properties of comment
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class Comment extends BaseEntity {
	private int commentId;
	private int newsId;
	private String commentText;
	private Timestamp creationDate;

	public Comment() {}
	
	public Comment (int commentId, int newsId, String commentText, 
			Timestamp creationDate) {
		this.commentId = commentId;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public boolean equals(Object obj) {
		Comment comment = (Comment)obj;
		return (this.commentId == comment.getCommentId()) && (this.newsId == comment.getNewsId()) 
				&& (this.commentText.equals(comment.commentText)) 
				&& (this.creationDate.toString().equals(comment.getCreationDate().toString()));
	}
	
}
