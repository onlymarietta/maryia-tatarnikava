package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface allows to perform operations with comments in database
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface AuthorDao extends BaseDao<Author> {

	/**
	 * Returns list of all authors
	 * 
	 * @param retrieveExpired if we want to get both expired and not expired authors
	 * @return list of authors
	 * @throws DaoException
	 */
	List<Author> getAll(boolean retrieveExpired) throws DaoException;

}
