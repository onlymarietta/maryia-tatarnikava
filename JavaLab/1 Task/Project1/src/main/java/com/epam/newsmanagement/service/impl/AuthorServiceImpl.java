package com.epam.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

public class AuthorServiceImpl implements AuthorService {

	static final Logger logger = LogManager.getLogger(Author.class.getName());
	private AuthorDao authorDao;

	public AuthorDao getAuthorDao() {
		return authorDao;
	}

	@Override
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	@Override
	public void create(Author author) throws ServiceException {
		try {
			logger.trace("Creating new author");
			authorDao.create(author);
		} catch (DaoException daoEx) {
			String error = "Can't create author";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		try {
			logger.trace("Updating author");
			authorDao.update(author);
		} catch (DaoException daoEx) {
			String error = "Can't update author";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void expire(Author author, Timestamp timestamp) throws ServiceException {
		try {
			logger.trace("Expiring author");
			author.setExpired(timestamp);
			authorDao.update(author);
		} catch (DaoException daoEx) {
			String error = "Can't make author expired";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<Author> getAll(boolean retrieveExpired) throws ServiceException {
		try {
			logger.trace("Getting all authors");
			return authorDao.getAll(retrieveExpired);
		} catch (DaoException daoEx) {
			String error = "Can't get all authors";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

}
