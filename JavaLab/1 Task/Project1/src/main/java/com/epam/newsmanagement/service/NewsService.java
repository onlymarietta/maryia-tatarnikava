package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.NewsTransferObj;
import com.epam.newsmanagement.entity.SearchCriteriaObj;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface allows to perform operations with news
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface NewsService {
	
	/**
	 * Sets object of news dao
	 * @param newsDao is object to set
	 */
	void setNewsDao(NewsDao newsDao);
	
	/**
	 * Sets object of author dao
	 * @param authorDao is object to set
	 */
	void setAuthorDao(AuthorDao authorDao);
	
	/**
	 * Sets object of comment dao
	 * @param commentDao is object to set
	 */
	void setCommentDao(CommentDao commentDao);
	
	/**
	 * Sets object of tag dao
	 * @param tagDao is object to set
	 */
	void setTagDao(TagDao tagDao);
	
	/**
	 * Creates news with author and tags
	 * @param news to create
	 * @param author to add to news
	 * @param listTag to add to news
	 * @throws ServiceException
	 */
	void create(News news, Author author, List<Tag> listTag) throws ServiceException;

	/**
	 * Returns news with its author, comments, tags
	 * @param newsId is id of news
	 * @return is an object of news with its author, comments, tags
	 * @throws ServiceException
	 */
	NewsTransferObj read(int newsId) throws ServiceException;
	
	/**
	 * Update news and tags
	 * @param news to update
	 * @param listTag to update
	 * @throws ServiceException
	 */
	void update(News news, List<Tag> listTag) throws ServiceException;
	
	/**
	 * Deletes news
	 * @param newsId is id of news to delete
	 * @throws ServiceException
	 */
	void delete(int newsId) throws ServiceException;

	/**
	 * Returns all news
	 * @return list of news
	 * @throws ServiceException
	 */
	List<News> getAll() throws ServiceException;
	
	/**
	 * Returns sorted list of most commented news
	 * @return list of sorted news
	 * @throws ServiceException
	 */
	List<News> listMostCommentedNews() throws ServiceException;

	/**
	 * Returns amount of all news
	 * @return amount of all news
	 * @throws ServiceException
	 */
	int countAll() throws ServiceException;
	
	/**
	 * Returns list of news according to search criteria
	 * @param criteria is search criteria
	 * @return list of news
	 * @throws ServiceException
	 */
	List<News> findByCriteria(SearchCriteriaObj criteria) throws ServiceException;
		
}
