package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

public class CommentServiceImpl implements CommentService {

	static final Logger logger = LogManager.getLogger(Comment.class.getName());
	private CommentDao commentDao;

	public CommentDao getCommentDao() {
		return commentDao;
	}

	@Override
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@Override
	public void create(Comment comment) throws ServiceException {
		try {
			logger.trace("Creating new comment");
			commentDao.create(comment);
		} catch (DaoException daoEx) {
			String error = "Can't create comment";
			logger.error(error);;
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void delete(int commentId) throws ServiceException {
		try {
			logger.trace("Deleting comment");
			commentDao.deleteById(commentId);
		} catch (DaoException daoEx) {
			String error = "Can't delete comment";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<Comment> getAll(int newsId) throws ServiceException {
		try {
			logger.trace("Getting all comments");
			return commentDao.getAll(newsId);
		} catch (DaoException daoEx) {
			String error =  "Can't get all comments";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

}
