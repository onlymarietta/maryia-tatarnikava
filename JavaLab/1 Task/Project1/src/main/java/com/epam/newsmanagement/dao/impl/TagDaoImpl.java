package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;

public class TagDaoImpl implements TagDao {
	
	private static final String SQL_CREATE = "insert into TAG (TAG_NAME) values (?)";
	private static final String SQL_GET_BY_ID = "select TAG_ID, TAG_NAME from TAG where TAG_ID = ?";
	private static final String SQL_UPDATE = "update TAG set TAG_NAME=? where TAG_ID=?";
	private static final String SQL_DELETE_TAG = "delete from TAG where TAG_ID = ?";
	private static final String SQL_DELETE_NEWS_TAG = "delete from NEWS_TAG where TAG_ID = ?";
	private static final String SQL_GET_LAST_INSERTED_ID = "select max(TAG_ID) from TAG";
	private static final String SQL_GET_ALL = "select TAG_ID, TAG_NAME from TAG";
	private static final String SQL_GET_ALL_NEWS_ID = "select TAG.TAG_ID, TAG.TAG_NAME from NEWS_TAG "
			+ "inner join TAG on NEWS_TAG.TAG_ID = TAG.TAG_ID where NEWS_ID = ?";

	private static final String COLUMN_TAG_ID = "TAG_ID";
	private static final String COLUMN_TAG_NAME = "TAG_NAME";
	
	private DataSource dataSource;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void create(Tag tag) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_CREATE)) {
			ps.setString(1, tag.getTagName());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't create tag: " + ex.getMessage(), ex);
		}
	}

	@Override
	public Tag getById(Integer id) throws DaoException {
		Tag tag = new Tag();
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_GET_BY_ID)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			tag = mapper(rs);
			rs.close();
			return tag;
		} catch (SQLException ex) {
			throw new DaoException("Can't get tag by id: " + ex.getMessage(), ex);
		}
	}

	@Override
	public void update(Tag tag) throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_UPDATE)) {
			ps.setString(1, tag.getTagName());
			ps.setInt(2, tag.getTagId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't update tag: " + ex.getMessage(), ex);
		}
	}
	
	@Override
	public void deleteById(Integer id) throws DaoException {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps1 = conn.prepareStatement(SQL_DELETE_TAG);
				PreparedStatement ps2 = conn.prepareStatement(SQL_DELETE_NEWS_TAG)) {
			ps1.setInt(1, id);
			ps1.executeUpdate();
			ps2.setInt(1, id);
			ps2.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException("Can't delete tag by id: " + ex.getMessage(), ex);
		}
	}
	
	@Override
	public int getLastInsertedId() throws DaoException {
		int insertedId = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(SQL_GET_LAST_INSERTED_ID)) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			insertedId = rs.getInt(1);
			return insertedId;

		} catch (SQLException ex) {
			throw new DaoException("Can't get last inserted tag id: " + ex.getMessage(), ex);
		}
	}
	
	@Override
	public List<Tag> getAll() throws DaoException {
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(SQL_GET_ALL)) {
			ResultSet rs = ps.executeQuery();
			List<Tag> listTag = new ArrayList<Tag>();
			while (rs.next()) {
				listTag.add(mapper(rs));
			}
			rs.close();
			return listTag;
		} catch (SQLException ex) {
			throw new DaoException("Can't get all tags: " + ex.getMessage(), ex);
		}
	}

	@Override
	public List<Tag> getAll(int newsId) throws DaoException{
		try (Connection conn = dataSource.getConnection(); 
				PreparedStatement ps = conn.prepareStatement(SQL_GET_ALL_NEWS_ID)) {
			ResultSet rs = ps.executeQuery();
			List<Tag> listTag = new ArrayList<Tag>();
			while (rs.next()) {
				listTag.add(mapper(rs));
			}
			rs.close();
			return listTag;
		} catch (SQLException ex) {
			throw new DaoException("Can't get all tags for one news: " + ex.getMessage(), ex);
		}
	}

	private Tag mapper(ResultSet rs) throws DaoException {
		try { 
			Tag tag = new Tag();
			tag.setTagId(rs.getInt(COLUMN_TAG_ID));
			tag.setTagName(rs.getString(COLUMN_TAG_NAME));
			return tag;
		} catch(SQLException ex) {
			throw new DaoException("Can't map into tag: " + ex.getMessage(), ex);
		}
	}

}
