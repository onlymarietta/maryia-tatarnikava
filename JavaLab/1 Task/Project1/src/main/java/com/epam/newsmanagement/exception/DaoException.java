package com.epam.newsmanagement.exception;

import java.sql.SQLException;

public class DaoException extends SQLException{

	private static final long serialVersionUID = 6340991418652592263L;

	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(String message, SQLException ex) {
		super(message, ex);
	}
	
}
