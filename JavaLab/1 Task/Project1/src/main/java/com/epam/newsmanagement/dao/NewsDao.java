package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.SearchCriteriaObj;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.entity.domainObject.News;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface allows to perform operations with news in database
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface NewsDao extends BaseDao<News> {	
	
	/**
	 * Returns all news
	 * @return list of news
	 * @throws DaoException
	 */
	List<News> getAll() throws DaoException;

	/**
	 * Returns amount of all news
	 * @return amount of news
	 * @throws DaoException
	 */
	int countAll() throws DaoException;

	/**
	 * Adds author to news
	 * @param newsId is id of the news
	 * @param authorId is id of author to add
	 * @throws DaoException
	 */
	void addAuthor(int newsId, int authorId) throws DaoException;
	
	/**
	 * Returns author of certain news
	 * @param newsId is id of news
	 * @return author
	 * @throws DaoException
	 */
	Author getAuthor(int newsId) throws DaoException;
	
	/**
	 * Adds tags to news
	 * @param newsId is id of news
	 * @param listTag is list of tags to add
	 * @throws DaoException
	 */
	void addTags(int newsId, List<Tag> listTag) throws DaoException;
	
	/**
	 * Deletes all tags of news
	 * @param newsId is id of news
	 * @throws DaoException
	 */
	void deleteAllTags(int newsId) throws DaoException;
	
	/**
	 * Searches news according to criteria
	 * @param criteria is search criteria
	 * @return list of news
	 * @throws DaoException
	 */
	List<News> findByCriteria(SearchCriteriaObj criteria) throws DaoException;

}
