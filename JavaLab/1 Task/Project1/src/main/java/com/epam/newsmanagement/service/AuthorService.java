package com.epam.newsmanagement.service;

import java.sql.Timestamp;
import java.util.List;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.domainObject.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface allows to perform operations with comments
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface AuthorService {
	
	/**
	 * Sets object of author dao
	 * @param authorDao is object to set
	 */
	void setAuthorDao(AuthorDao authorDao);
	
	/**
	 * Creates new author
	 * @param author to create
	 * @throws ServiceException
	 */
	void create(Author author) throws ServiceException;
	
	/**
	 * Updates author
	 * @param author to update
	 * @throws ServiceException
	 */
	void update(Author author) throws ServiceException;

	/**
	 * Expires author
	 * @param author to expire
	 * @param timeetamp of expiration
	 * @throws ServiceException
	 */
	void expire(Author author, Timestamp timeetamp) throws ServiceException;
	
	/**
	 * Returns all authors
	 * @param retrieveExpired whether return expired author or not
	 * @return list of authors
	 * @throws ServiceException
	 */
	List<Author> getAll(boolean retrieveExpired) throws ServiceException;

}
