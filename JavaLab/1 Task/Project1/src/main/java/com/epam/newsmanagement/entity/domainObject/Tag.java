package com.epam.newsmanagement.entity.domainObject;

/**
 * Class contains properties of tag
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class Tag extends BaseEntity {
	private int tagId;
	private String tagName;
	
	public Tag() {}
	
	public Tag (int tagId, String tagName) {
		this.tagId = tagId;
		this.tagName = tagName;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	public boolean equals(Object obj) {
		Tag tag = (Tag)obj;
		return this.tagId == tag.getTagId() && this.tagName.equals(tag.getTagName());
	}
	
}
