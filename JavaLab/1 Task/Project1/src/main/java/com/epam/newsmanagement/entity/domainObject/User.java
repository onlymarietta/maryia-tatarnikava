package com.epam.newsmanagement.entity.domainObject;

/**
 * Class contains properties of user
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public class User extends BaseEntity {
	private int userId;
	private String userName;
	private String login;
	private String password;
	private String role;
	
	public User (int userId, String userName, String login, String password, String role){
		this.userId = userId;
		this.userName = userName;
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role){
		this.role = role;
	}
	
}
