package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface allows to perform operations with comments
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface CommentService {
	
	/**
	 * Sets object of comment dao
	 * @param commentDao is object to set
	 */
	void setCommentDao(CommentDao commentDao);
	
	/**
	 * Creates new comment
	 * @param comment to create
	 * @throws ServiceException
	 */
	void create(Comment comment) throws ServiceException;
	
	/**
	 * Deletes comment by its id
	 * @param commentId is id of comment
	 * @throws ServiceException
	 */
	void delete(int commentId) throws ServiceException;
	
	/**
	 * Returns all comment for certain news
	 * @param newsId is id of news
	 * @return list of comments
	 * @throws ServiceException
	 */
	List<Comment> getAll(int newsId) throws ServiceException;

}
