package com.epam.newsmanagement.utils;

import java.sql.SQLException;
import java.util.Comparator;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.domainObject.News;

public class NewsComparator implements Comparator<News> {
	
	private CommentDao commentDao;
	
	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	@Override
	public int compare(News news1, News news2) {
		int result = -1;
		try {
			result = commentDao.countAll(news1.getNewsId()) - commentDao.countAll(news2.getNewsId());
		} catch (SQLException ex) {
		}
		return result;
	}

}
