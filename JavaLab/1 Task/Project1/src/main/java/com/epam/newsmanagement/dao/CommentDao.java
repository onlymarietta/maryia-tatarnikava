package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.domainObject.Comment;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface allows to perform operations with comments in database
 * 
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface CommentDao extends BaseDao<Comment> {

	/**
	 * Returns all comments for the certain news
	 * @param newsId is id of news
	 * @return list of all comments of one news
	 * @throws DaoException
	 */
	List<Comment> getAll(int newsId) throws DaoException;

	/**
	 * Returns amount of all comments for certain news
	 * @param newsId is id of the news
	 * @return amount of comments
	 * @throws DaoException
	 */
	int countAll(int newsId) throws DaoException;

}
