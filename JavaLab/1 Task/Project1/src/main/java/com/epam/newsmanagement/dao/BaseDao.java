package com.epam.newsmanagement.dao;

import javax.sql.DataSource;

import com.epam.newsmanagement.entity.domainObject.BaseEntity;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface allows to perform basic operations in database
 *
 * @author Maryia_Tatarnikava
 * @version 1.0
 */

public interface BaseDao<T extends BaseEntity> {

	/**
	 * Set the datasource
	 * 
	 * @param ds to set datasource
	 * @throws DaoException
	 */
	void setDataSource(DataSource ds) throws DaoException;

	/**
	 * Creates new entity in database
	 * 
	 * @param entity to create
	 * @throws DaoException
	 */
	void create(T entity) throws DaoException;

	/**
	 * Returns entity by its id
	 * 
	 * @param id of the entity
	 * @return entity
	 * @throws DaoException
	 */
	T getById(Integer id) throws DaoException;

	/**
	 * Replace entity with the new one
	 * 
	 * @param entity to replace with
	 * @throws DaoException
	 */
	void update(T entity) throws DaoException;

	/**
	 * Delete entity using its id
	 * 
	 * @param id of entity to delete
	 * @throws DaoException
	 */
	void deleteById(Integer id) throws DaoException;

	/**
	 * Get id of the last inserted entity
	 * 
	 * @return last inserted id
	 * @throws DaoException
	 */
	int getLastInsertedId() throws DaoException;

}
