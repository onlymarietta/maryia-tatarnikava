package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.domainObject.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

public class TagServiceImpl implements TagService {

	static final Logger logger = LogManager.getLogger(Tag.class.getName());
	private TagDao tagDao;

	public TagDao getTagDao() {
		return tagDao;
	}

	@Override
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	@Override
	public void create(Tag tag) throws ServiceException {
		try {
			logger.trace("Creating new tag");
			tagDao.create(tag);
		} catch (DaoException daoEx) {
			String error = "Can't create tag";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			logger.trace("Updating tag");
			tagDao.update(tag);
		} catch (DaoException daoEx) {
			String error = "Can't update tag";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public void delete(int tagId) throws ServiceException {
		try {
			logger.trace("Deleting tag");
			tagDao.deleteById(tagId);
		} catch (DaoException daoEx) {
			String error = "Can't delete tag";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

	@Override
	public List<Tag> getAll() throws ServiceException {
		try {
			logger.trace("Getting all tags");
			return tagDao.getAll();
		} catch (DaoException daoEx) {
			String error = "Can't get all tags";
			logger.error(error);
			throw new ServiceException(error, daoEx);
		}
	}

}
